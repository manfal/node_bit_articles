const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const mongoConnection = require('connect-mongo')(session);
const path = require('path');
const promisify = require('es6-promisify');
const routes = require('./routes/index');
const helpers = require('./helpers/index');

const app = express();

// setup views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// setup static files

app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
	res.locals.helpers = helpers;
	next();
});

app.use('/', routes);


module.exports = app;
