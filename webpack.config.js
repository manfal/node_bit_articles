// webpack.config.js
// Configure CSS processing & injection
//  const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const config = {
	entry: './public/js/index.js',
	output: {
		// Webpack prefers an absolute path:
		path: path.resolve(__dirname, './public/dist/'),
		filename: 'app.bundle.js',
	},
	module: {
		rules: [
			{
				// Uses regex to test for a file type - in this case, ends with `.css`
				test: /\.css$/,
				// Apply these loaders if test returns true
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader'),
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
			},
		],
	},
	plugins: [
		new ExtractTextPlugin('app.css'),
	],
};

// process.noDeprecation = true;

module.exports = config;
