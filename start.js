const mongoose = require('mongoose');
const dotevn = require('dotenv');
const app = require('./app');

dotevn.config({ path: '.env' });

mongoose.connect(process.env.DATABASE);
mongoose.Promise = global.Promise;
mongoose.connection.on('error', (error) => {
	console.log('An Error occured during DB connection', error);
});

// setup modules below
app.set('port', process.env.PORT || 3001);
const server = app.listen(app.get('port'), () => {
	console.log(`App Listening on ${app.get('port')}`);
});

